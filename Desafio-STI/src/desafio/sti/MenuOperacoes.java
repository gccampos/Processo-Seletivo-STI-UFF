package desafio.sti;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

//@author guilherne
public class MenuOperacoes {

    public static void menuPrincipal() throws IOException {
        OperadorCSV operadorCSV = new OperadorCSV();
        Scanner scanner = new Scanner(System.in);
        OperadorEmail operadorEmail = new OperadorEmail();
        ArrayList<Aluno> alunos = operadorCSV.manipulaArquivo(); //executando as operações referentes ao CSV
        boolean achou = false; // flag para saber se achou matrícula na lista
        String matriculaEntrada; //Input de matricula do usuario
        
        System.out.println("Bem vindo ao módulo de criação de UFF mail");
        System.out.println("Entre com a sua matrícula:");
        matriculaEntrada = scanner.next();
        
        for (Aluno aluno : alunos) { //Percorre a lista de alunos
            if (!aluno.getMat().equalsIgnoreCase(matriculaEntrada)) { //Eu realmente não sei se é necessário usar esse if
                continue; //entretanto, não consegui de outra forma.
            }
            if (aluno.getMat().equalsIgnoreCase(matriculaEntrada)) { //Se a matricula informada foi achada 
                achou = true; //flag = true
                if (aluno.getStatus().equalsIgnoreCase("Inativo")) { //Caso aluno esteja inativo
                    System.out.println("Aluno Inativo, por favor entre com uma matrícula de aluno ativo");
                    menuPrincipal();
                }
                if (aluno.isUffmailAtivo() == true) { //Se ja possui uffmail
                    System.out.println("Usuário já possui UffMail");
                    menuPrincipal();
                }
                if (aluno.isUffmailAtivo() == false) { //Se não possui entao 
                    operadorEmail.listarEmailsDisponiveis(aluno); //lista as opções de email
                }
            }
        }
        if (achou == false) { //Caso não ache a matrícula, que também serve caso a entrada não seja uma matricula
            System.out.println("Matricula não encontrada, por favor entre com uma matrícula existente:");
            menuPrincipal();
        }
    }

}
