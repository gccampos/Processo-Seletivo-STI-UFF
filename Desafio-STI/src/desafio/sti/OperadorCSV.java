package desafio.sti;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

//Essa classe tem como objetivo manipular dos dados do arquivo CSV
public class OperadorCSV {

    //Esse método percorre o arquivo e insere os dados em um ArrayList<Aluno>
    public ArrayList manipulaArquivo() throws IOException {

        String caminhoArquivo = "/home/guilherme/Processo-Seletivo-STI-UFF/alunos.csv"; //Caminho do arquivo
        BufferedReader leitor = new BufferedReader(new FileReader(caminhoArquivo)); // Eu não necessariamente preciso da variavel CSValunos, porém, achei que assim ficava melhor 
        String iteradorLinha = leitor.readLine(); //Leitor das linhas do arquivo
        ArrayList<Aluno> alunos = new ArrayList<>();

        while ((iteradorLinha = leitor.readLine()) != null) {  //Percorre as linhas 
            String[] linha = iteradorLinha.split(",");  //Identificador 
            Aluno aluno = new Aluno(); //variável temporária apenas para armazenar os dados antes de inseri-los na lista
            aluno.setNome(linha[0]); //seta os atributos de acordo com a coluna correspondente.
            aluno.setMat(linha[1]);
            aluno.setTel(linha[2]);
            aluno.setEmail(linha[3]);
            aluno.setUffmail(linha[4]);
            aluno.setStatus(linha[5]);
            alunos.add(aluno); //adiciona na lista 'alunos'
        }

        leitor.close(); //Fecha o arquivo

        return alunos;
    }
}
