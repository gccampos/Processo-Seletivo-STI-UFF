/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desafio.sti;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author guilherme
 */
public class OperadorEmail {

    public void listarEmailsDisponiveis(Aluno aluno) {
        OperadorSms operadorSms = new OperadorSms();
        int entradaUsuario = 0; //User Input
        Scanner scanner = new Scanner(System.in);
        String SelecEmail; //Email selecionado, variavel para evitar repetição de código
        HashMap<Integer, String> opcoes = gerarOpcoes(aluno);
        
        System.out.println("Módulo de criação de UffMail:");
        System.out.println("Escolha uma das opcoes abaixo:");
        
        for (Map.Entry<Integer, String> opcao : opcoes.entrySet()) {
            System.out.println(opcao.getKey() + ". " + adicionaDominio(opcao.getValue()));
        }
        try {
            entradaUsuario = scanner.nextInt();
            switch (entradaUsuario) {
                case 1:
                    SelecEmail = adicionaDominio(opcoes.get(1));
                    mensagemCriacao(SelecEmail);
                    operadorSms.enviaSMS(aluno);
                    break;
                case 2:
                    SelecEmail = adicionaDominio(opcoes.get(2));
                    mensagemCriacao(SelecEmail);
                    operadorSms.enviaSMS(aluno);
                    break;
                case 3:
                    SelecEmail = adicionaDominio(opcoes.get(3));
                    mensagemCriacao(SelecEmail);
                    operadorSms.enviaSMS(aluno);
                    break;
                case 4:
                    SelecEmail = adicionaDominio(opcoes.get(4));
                    mensagemCriacao(SelecEmail);
                    operadorSms.enviaSMS(aluno);
                    break;
                default:
                    listarEmailsDisponiveis(aluno);
            }
        } catch (InputMismatchException e) { //Se a entrada não for válida para o contexto, ele repete a lista dos emails
            System.out.println("Entrada inválida, entre com uma opção da lista");
            listarEmailsDisponiveis(aluno);
        }
    }

    public String adicionaDominio(String opcao) {
        return opcao + "@id.uff.br";
    }

    public String mensagemCriacao(String SelecEmail) {
        return "A criação de seu e-mail " + SelecEmail + " será feita nos próximos minutos";
    }

    public HashMap gerarOpcoes(Aluno aluno) {
        
        String[] nomeCompleto = aluno.getNome().split(" "); // pega o nome completo da pessoa e divide por palavras, sendo o separador um espaço
        HashMap<Integer, String> opcoes = new HashMap<>();
        String opcao1 = nomeCompleto[0] + nomeCompleto[1];
        String opcao2 = nomeCompleto[1] + nomeCompleto[0];
        String opcao3 = nomeCompleto[0] + nomeCompleto[2];
        String opcao4 = nomeCompleto[2] + nomeCompleto[0];
        opcoes.put(1, opcao1);
        opcoes.put(2, opcao2);
        opcoes.put(3, opcao3);
        opcoes.put(4, opcao4);
        return opcoes;
    }
}
