package desafio.sti;
// @author guilherme

public class Aluno {

    private String nome;
    private String matricula;
    private String telefone;
    private String email;
    private String uffMail;
    private String status;

    public String getNome() {
        return this.nome;
    }

    public String getMat() {
        return this.matricula;
    }

    public String getTel() {
        return this.telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public String getUffmail() {
        return this.uffMail;
    }

    public String getStatus() {
        return this.status;
    }

    public String isAtivo() {
        return this.status;
    }

    public boolean isUffmailAtivo() {
        if(uffMail.equals("")) return false;
        return true;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMat(String matricula) {
        this.matricula = matricula;
    }

    public void setTel(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUffmail(String uffMail) {
        this.uffMail = uffMail;
    }

    public void setStatus(String status) {
        this.status = status;
    }      
}

